package za.co.bmw.app;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "GetPrimes", urlPatterns = {"/GetPrimes"})
public class GetPrimes extends HttpServlet {

  private String calculatePrimes(int a, int b) {
    StringBuilder sb = new StringBuilder();
    int s3, flag = 0, i, j;
    //String displayPrimes = "";


    // Calculate prime numbers here and add to StringBuilder output:
    sb.append("Prime Numbers");
    sb.append("<br>");
    sb.append("<ul>");

    for(i = a; i < b; i++)
    {
      for(j = 2; j < i; j++)
      {
        if (i % j == 0) {
          flag = 0;
          break;
        } else {
          flag = 1;
        }
      }
        if (flag == 1)
        {
          sb.append("<li>");
          sb.append(i);
          sb.append("</li>");
        }

    }
    sb.append("</ul>");

    return sb.toString();
  }

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");

    int lowerNumber = Integer.parseInt(request.getParameter("lowerNumber"));
    int upperNumber = Integer.parseInt(request.getParameter("upperNumber"));

    try (PrintWriter out = response.getWriter()) {
      out.println(calculatePrimes(lowerNumber, upperNumber));
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
